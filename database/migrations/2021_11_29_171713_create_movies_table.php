<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->date('data_premiere')->nullable();
            $table->string('sinopsis');
            $table->string('duration')->nullable();
            $table->enum('type', ['movie', 'serie']);
            $table->string('urlFile')->nullable();
            $table->string('imagen')->nullable();
            $table->bigInteger('season')->nullable();
            $table->bigInteger('chapter')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
