<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ApiMovController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Movie;
use App\Http\Resources\MovieResource;
use App\Http\Resources\MovieCollection;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
/*
use App\Models\Genre;
use App\Http\Resources\GenereResource;
use App\Http\Resources\GenereCollection;
*/

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


///////////////////
//// movies   /////
///////////////////






/**here get all movies */


/**
 * Group middlewares
 */
/**
 * Get all movies
 */
/*
Route::get('/movie', function () {
    return new MovieCollection(Movie::all());
});
*/

/** GET MOVIE BY ID GOES HERE  */








///////////////////////////
/**
 * Public routes
 */
////////////////////////
////////////////////////
/////////////////////////////////////////////////////////

///////////////////
//// users   /////
///////////////////

///////////////////
//// USER auth methods   /////
///////////////////





/**
 * POST/register user
 * Creates a new user
 * Checks if a user with the same email already exists
 * returns the movie if everything is OK
 * returns Error with 400 code
 */
Route::post('/user', function (Request $request) {
    $users = new UserCollection(User::all());
    foreach ($users as $user) {
        if ($user->email == $request->email)
            return response()->json(['Status' => "Error: email already in the DB, must be UNIQUE!"], 400);
    }
    return new UserResource(User::create($request->all()));
});


///////////////////////////
/**
 * Private routes
 */
////////////////////////
////////////////////////

///////////////////
//// users   /////
///////////////////

/**
 * Get all users
 */
Route::get('/user', function () {
    return new UserCollection(User::all());
});

/**
 * Get user by ID
 * format: user/id
 */
Route::get('/user/{id}', function ($id) {
    try {
        return new UserResource(User::findOrFail($id));
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status' => "Error: movie doesn't exist!"], 404);
    }
});




/**
 * Delete user (partial update)
 * format: user/id
 */
Route::delete('/user/{id}', function ($id) {
    try {
        $product = User::findOrFail($id);
    } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
        return response()->json(['Status' => "Error: user doesn't exist!"], 404);
    }
    $product->delete();
    return response()->json(['Status' => 'Success!'], 200);
});




/**
 * Protected Routes for users with access token(sanctum)
 */
Route::middleware(['auth:sanctum'])->group(function () {
    /**
     * GET movies
     * /movie
     * Get movies if user loged in
     * returns: movie collection
     * Type: GET
     */
    Route::get('/movie', function () {
        return new MovieCollection(Movie::all());
    });
    /**
     * Logout
     * /logout
     * Registers user if credentials pass validation.
     * Calls controller, method login
     * Type: POST
     */
    Route::post('/logout', [AuthController::class, 'logout']);
});




/**
 * Protected Routes for users with access token(sanctum)
 */
Route::middleware(['auth:sanctum', 'abilities:admin-role'])->group(function () {
    /**
     * Get movies by ID
     * format: movie/id
     */
    Route::get('/movie/{id}', function ($id) {
        try {
            return new MovieResource(Movie::findOrFail($id));
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Status' => "Error: movie doesn't exist!"], 404);
        }
    });
});




/*▂▃▅▇█▓▒░ Public routes ░▒▓█▇▅▃▂*/
/**
 * THESE ROUTES ARE PUBLIC AND REQUIRE NO ACCESS TOKEN
 */

/*▂▅█▓▒░ Users ░▒▓█▅▂*/


/**
 * Register
 * /register
 * Registers user in if credentials pass validation and credentials match.
 * Calls controller, method register
 * @method: POST
 * @unauthenticated
 * @param String name
 * @param String email (unique in User)
 * @param String password (hashed with bcrypt)
 * @param String password_confirm (validated against password)
 * NOTE: all @params required
 * NOTE: field validation in AuthController
 * @method register
 * @return User object on success (without password)
 * @return Object containing validation Error (for example, email not unique)
 */
Route::post('/register', [AuthController::class, 'register']);


/**
 * Login
 * /login
 * Logs user in if credentials pass validation and credentials match.
 * Calls controller, method register
 * @method: POST
 * @unauthenticated
 * @param String email (this time without the unique validation)
 * @param String password (hashed with bcrypt)
 * @param String password_confirmation (validated against password)
 * NOTE: all @params required
 * NOTE: field validation in AuthController
 * @method login
 * @return User object on success (without password)
 * @return Object containing validation Error (for example, email not unique)
 */
Route::post('/login', [AuthController::class, 'login']);


/**
 * Admin Login
 * /admin/login
 * Logs in existing hardcoded (in User seeder) if credentials pass validation.
 * Calls controller, method adminlogin
 * @method: POST
 * @unauthenticated
 * @param String email (this time without the unique validation)
 * @param String password (hashed with bcrypt)
 * @param String password_confirmation (validated against password)
 * NOTE: all @params required
 * NOTE: field validation in AuthController
 * @method logadminloginin
 * @return User object on success (without password)
 * @return Object containing validation Error (for example, User doesn't have admin role)
 */
Route::post('/admin/login', [AuthController::class, 'adminlogin']);




/*▂▃▅▇█▓▒░ Private routes (requrie login with sanctum and user role) ░▒▓█▇▅▃▂*/

/**
 * Protected Routes for users with access token(sanctum)/user role
 */

/*▂▅█▓▒░ Users ░▒▓█▅▂*/

Route::middleware(['auth:sanctum'])->group(function () {


    /**
     * Logout
     * /logout
     * Registers user if credentials pass validation.
     * Calls controller, method logout
     * @method: POST
     * @authenticated
     * @header Accept application/json
     * @auth Bearer token
     * @param String email (this time without the unique validation)
     * NOTE: single @params required
     * @method logout
     * @return User object on success (without password)
     * @return Object containing validation Error (for example, User doesn't have admin role)
     */
    Route::post('/logout', [AuthController::class, 'logout']);


    /**
     * Update user (whole)
     * /user/{id}
     * @method: GET
     * @authenticated
     * @header Accept application/json
     * @auth Bearer token
     * @urlParam id int
     * @return UserResource json object with a single user with the ID given
     * @return Object with error and status 404 on ID not found
     */
    Route::put('/user/{id}', function (Request $request, $id) {
        try {
            Movie::findOrFail($id)->update($request->all());
            return new UserResource(User::findOrFail($id));
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Status' => "Error: User doesn't exist!"], 404);
        }
    });


    /**
     * Patch user (partial update)
     * format: user/id
     * @method: GET
     * @authenticated
     * @header Accept application/json
     * @auth Bearer token
     * @urlParam id int
     * @return UserResource json object with a single user with the ID given
     * @return Object with error and status 404 on ID not found
     */
    Route::patch('/user/{id}', function (Request $request, $id) {
        try {
            Movie::findOrFail($id)->update($request->all());
            return new UserResource(User::findOrFail($id));
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Status' => "Error: User doesn't exist!"], 404);
        }
    });


    /*▂▅█▓▒░ Movies ░▒▓█▅▂*/


    /**
     * Get movies by ID
     * /movie/{id}
     * @method: GET
     * @authenticated
     * @header Accept application/json
     * @auth Bearer token
     * @urlParam id int
     * @return MovieResource json object with a single movie with the ID given
     * @return Object with error and status 404 on ID not found
     */
    Route::get('/movie/{id}', function ($id) {
        try {
            return new MovieResource(Movie::findOrFail($id));
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Status' => "Error: movie doesn't exist!"], 404);
        }
    });


    /**
     * Search movies by Attribute
     * /movie/search
     * @method: POST
     * @authenticated
     * @header Accept application/json
     * @auth Bearer token
     * @param String title
     * @param String sinopsis
     * @param String genere
     * @param String type(movie/serie)
     * @param String directors
     * @param String actors
     * NOTE: All @params are optional but there must be at least one or else error returns
     * @return MovieCollection json object with a list of movies matching search @params
     * @return Object with error and status 404 on match not found
     */
    Route::post('/movie/search', [ApiMovController::class, 'search']);
});




/*▂▃▅▇█▓▒░Admin routes (requrie admin login/role)░▒▓█▇▅▃▂*/
/**
 * Protected Routes for users with access token(sanctum)
 */
Route::middleware(['auth:sanctum', 'abilities:admin-role'])->group(function () {
    /*▂▅█▓▒░ Movies (only) ░▒▓█▅▂*/
    /**
     * GET movies
     * /movie
     * Get all movies if user is Admin role
     * @method: GET
     * @authenticated
     * @role admin
     * @header Accept application/json
     * @auth Bearer token
     * @return MovieCollection json object with all the movies in the DB
     */
    Route::get('/movie', function () {
        return new MovieCollection(Movie::all());
    });

    /**
     * POST movie
     * /movie
     * Creates a new movie
     * Checks if a movie with the same title already exists
     * returns the movie if everything is OK
     * returns Error with 400 code
     * @method: POST
     * @authenticated
     * @role admin
     * @header Accept application/json
     * @auth Bearer token
     * @return MovieResource json object with the created Movie
     */
    Route::post('/movie', function (Request $request) {
        $movies = new MovieCollection(Movie::all());
        foreach ($movies as $movie) {
            if ($movie->title == $request->title)
                return response()->json(['Status' => "Error: title already in the DB, must be UNIQUE!"], 406);
        }
        return new MovieResource(Movie::create($request->all()));
    });

    /**
     * Update movie (whole)
     * format: movie/id
     */
    Route::put('/movie/{id}', function (Request $request, $id) {
        try {
            Movie::findOrFail($id)->update($request->all());
            return new MovieResource(Movie::findOrFail($id));
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Status' => "Error: movie doesn't exist!"], 404);
        }
    });


    /**
     * Patch movie (partial update)
     * format: movie/id
     */
    Route::patch('/movie/{id}', function (Request $request, $id) {
        try {
            Movie::findOrFail($id)->update($request->all());
            return new MovieResource(Movie::findOrFail($id));
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Status' => "Error: movie doesn't exist!"], 404);
        }
    });


    /**
     * Delete movie (partial update)
     * format: movie/id
     */
    Route::delete('/movie/{id}', function ($id) {
        try {
            $product = Movie::findOrFail($id);
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Status' => "Error: movie doesn't exist!"], 404);
        }
        $product->delete();
        return response()->json(['Status' => 'Success!'], 200);
    });
});
