<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;
    public function movies()
    {
        return $this->belongsToMany(Movie::class, 'movies_x_genres', 'id_genre', 'id_movie');
    }
}
