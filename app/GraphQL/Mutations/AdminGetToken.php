<?php

namespace App\GraphQL\Mutations;
use App\Models\User;

class AdminGetToken
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        // TODO implement the resolver
        $user = User::where('email', 'h4x02-admin@petflix.com')->first();
        $token = $user->createToken('api-token', ['user-role', 'admin-role'])->plainTextToken;
        return (object)['email' => $args['email'], 'token' => $token];
    }
}
