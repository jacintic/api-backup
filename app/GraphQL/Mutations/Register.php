<?php

namespace App\GraphQL\Mutations;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class Register
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        // TODO implement the resolver
        //return "Hello, {$args['name']}!";

        $user = User::create([
            'name' => $args['name'],
            'email' => $args['email'],
            'password' => bcrypt($args['password'])
        ]);
        $token = $user->createToken('api-token', ['user-role'])->plainTextToken;
        return (object)['email' => $args['email'], 'token' => $token];
    }
}
