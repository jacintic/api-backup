<?php

namespace App\GraphQL\Mutations;

class TestII
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        // TODO implement the resolver
        return "Hello, {$args['name']}!";
    }
}
