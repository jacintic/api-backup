<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * register method
     * /register
     * encrypts password with bcrypt
     * @param string name
     * @param string email
     * @param string password
     * @param string password_confirmation
     * returns username and plaintext token for Auth
     */
    public function register(Request $request)
    {
        /*▂▅█▓▒░ Validation ░▒▓█▅▂*/
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => [
                'required',
                'between:7,25',
                'regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*(_|[^\w])).+$/',
                'confirmed'
               ]
        ];

        $customMessages = [
            'required' => 'The :attribute field is required.',
            'regex' => 'The passowrd must contain small caps, capital leters, at least a number and a special character'
        ];

        $this->validate($request, $rules, $customMessages);

        /*▂▅█▓▒░ Insert in DB ░▒▓█▅▂*/
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        /*▂▅█▓▒░ Token creation ░▒▓█▅▂*/
        $token = $user->createToken('api-token', ['user-role'])->plainTextToken;

        /*▂▅█▓▒░ Response ░▒▓█▅▂*/
        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201); // 201 => created
    }

    /**
     * login method
     * @param string email
     * @param string password
     * @param string password_confirmation
     * @return object (username and plaintext token for Auth and status code 200)
     */
    public function login(Request $request)
    {
        //
        $fields = $request->validate([
            // email can't be unique or it will try to match against DB
            'email' => 'required|email',
            'password' => 'required|string|confirmed'
        ]);
        // get the user by mail(unique)
        $user = User::where('email', $fields['email'])->first();
        // check for user, return not found if not found
        if (!$user) {
            return response(['Error' => 'User not found'], 404); // 404 => not found
        }
        // check credentials (pw, mail) return unauthorized if they don't match
        if (!Hash::check($fields['password'], $user->password)) {
            return response(['Error' => 'Wrong credentials'], 401); // 401 => unauthorized
        }
        $token = $user->createToken('api-token', ['user-role'])->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 202); // 202 => accepted
    }

    /**
     * Login method
     * @param String user's email
     * @return Object (user's email, status message, code 200)
     */
    public function logout(Request $request)
    {
        // get the user by mail(unique)
        $user = User::where('email', $request->email)->first();
        // delete current token(sent with the request)
        $request->user()->currentAccessToken()->delete();

        // format response with the desired info(email and status)
        $response = [
            'user' => $user->email,
            'Status' => 'Successfully logged out!'
        ];
        // return response
        return response($response, 200);
    }

    /**
     * login method
     * @param string email
     * @param string password
     * @param string password_confirmation
     * @return object (username and plaintext token for Auth and status code 200)
     */
    public function adminlogin(Request $request)
    {
        //
        $fields = $request->validate([
            // email can't be unique or it will try to match against DB
            'email' => 'required|email',
            'password' => 'required|string|confirmed'
        ]);
        // get the user by mail(unique)
        $user = User::where('email', $fields['email'])->first();
        // check for user, return not found if not found
        if (!$user) {
            return response(['Error' => 'User not found'], 404); // 404 => not found
        }
        // check credentials (pw, mail) return unauthorized if they don't match
        if (!Hash::check($fields['password'], $user->password)) {
            return response(['Error' => 'Wrong credentials'], 401); // 401 => unauthorized
        }
        // check if user is admin
        // this is where you'd issue a ban or timeout
        if (!$user->admin) {
            return response(['Error' => 'We found an intruder! You don\'t have admin permission.'], 403); // 403 => forbidden
        }
        $token = $user->createToken('api-token', ['user-role', 'admin-role'])->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 202); // 202 => accepted
    }
}
