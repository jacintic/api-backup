<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\Http\Resources\MovieResource;
use App\Http\Resources\MovieCollection;
use App\Htpp\Controllers\Builder;

class ApiMovController extends Controller
{
    /**
     * Login method
     * @param Object $request
     * @return Object collection of movies
     */
    public function search(Request $request)
    {
        // validaton at least one field is required for the request to go through
        $fields = $request->validate([
            'title' => 'string|required_without_all:genere,type,director,actor',
            'genere' => 'required_without_all:title,type,director,actor:',
            'type' => 'string|required_without_all:title,genere,director,actor',
            'director' => 'string|required_without_all:title,genere,type,actor',
            'actor' => 'string|required_without_all:title,genere,type,director'
        ]);

        // se genera una query a la que se le añaden filtros acumulativamente
        // ej. filtrar por type y genere
        $movie = Movie::query();
        /**
         * $request->title
         * es el termino de busqueda por titulo (p.e. spiderman)
         * cuando desde la home buscamos en la navbar, al cargar la vista search
         * esta se rellena con el termino recivido desde homecontroller -> searchTerm
         *
         * EXPLICACIÓN DE IFs
         * en este controller con los IFs se comprueva si el campo contiene info o no
         * si el campo contiene info se filtra por ese campo acumulativamente
         */
        if ($request->title)
            $movies = $movie->title($request->title);
        // join nsecesario para campos: genero,tipo,director y actor
        $movies = $movie->defaultJoin();
        // explicación de whereIn: los campos type y genere toman un array que almacena el valor/valores totales
        // para filtrar por mas de un valor
        if ($request->type)
            $movies = $movie->type($request->type);
        if ($request->genere) {
            $movies = $movie->genereIn($request->genere);
        }
        if ($request->director)
            $movies = $movie->director($request->director);
        if ($request->actor){
            $movies= $movie->actorJoin();
            $movies = $movie->actor($request->actor);
        }

        // get movie collection
        $movieCollection = $movies->distinct()->get();

        // report no movies found

        if(count($movieCollection) == 0)
            return response(["Error" => "No movies found matching the criteria."], 404); // 404 => not found

        $response = [
            'movies' => $movieCollection
        ];

        return response($response, 200); // 200 => OK
    }
}
