<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'sinopsis' => $this->sinopsis,
            'type' => $this->type,
            'generes' => new GenereCollection($this->genere),
            'directors' => new DirectorCollection($this->director),
            'actors' => new ActorCollection($this->actor)
        ];
    }
}
